<?php

namespace Drupal\cyberduck_spotify\Access;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;

/**
 * Checks access for displaying Artist Information Page.
 */
class AccessCheck implements AccessInterface {

    /**
     * A custom access check.
     *
     * @param \Drupal\Core\Session\AccountInterface $account
     *   Run access checks for this account.
     *
     * @return \Drupal\Core\Access\AccessResultInterface
     *   The access result.
     */
    public function access(AccountInterface $account) {

        return ($account->hasPermission('Artist Page Access') ? AccessResult::allowed() : AccessResult::forbidden());

    }

}