<?php

namespace Drupal\cyberduck_spotify\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class SpotifyArtistController.
 */
class SpotifyArtistController extends ControllerBase {

  /**
   * Drupal\cyberduck_spotify\Service\SpotifyFetchService definition.
   *
   * @var \Drupal\cyberduck_spotify\Service\SpotifyFetchService
   */
  protected $cyberduckSpotifySpotifyApi;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->cyberduckSpotifySpotifyApi = $container->get('cyberduck_spotify.spotify_api');
    return $instance;
  }

  public function launchPage($id) {

      if ($this->cyberduckSpotifySpotifyApi->checkConnection()) {

          \Drupal::logger('cyberduck_spotify')->notice('Connection to Artist Information Page @id',
              [
                  '@id' => $id,
              ]);

          $artist = $this->cyberduckSpotifySpotifyApi->fetchArtistInformation($id);

          return [
              '#theme' => 'artist_information',
              '#type' => 'markup',
              '#artist' => $artist,
              '#attached' => [
                  'library' => [
                      'cyberduck_spotify/cyberduck-spotify-theme'
                  ]
              ],
              '#cache' => [
                  'max-age' => 0
              ]
          ];

      } else {

          \Drupal::logger('cyberduck_spotify')->error('Connection failed on Artist Information Page @id',
              [
                  '@id' => $id,
              ]);
          throw new AccessDeniedHttpException();

      }

  }
}
