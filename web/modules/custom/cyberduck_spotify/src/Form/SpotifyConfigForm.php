<?php

namespace Drupal\cyberduck_spotify\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SpotifyConfigForm.
 */
class SpotifyConfigForm extends ConfigFormBase {

  /**
   * Drupal\cyberduck_spotify\Service\SpotifyFetchService definition.
   *
   * @var \Drupal\cyberduck_spotify\Service\SpotifyFetchService
   */
  protected $cyberduckSpotifySpotifyApi;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->cyberduckSpotifySpotifyApi = $container->get('cyberduck_spotify.spotify_api');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cyberduck_spotify.spotifyconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'spotify_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

      $config = $this->config('cyberduck_spotify.spotifyconfig');

      if (!$this->cyberduckSpotifySpotifyApi->checkConnection()) {
          \Drupal::logger('cyberduck_spotify')->error('Failed to connect to Spotify API');
          $form['connection_failed'] = [
              '#type' => 'markup',
              '#markup' => 'Connection to Spotify API failed, please check your credentials are correct',
              '#weight' => '0',
          ];
      } else {
          \Drupal::logger('cyberduck_spotify')->notice('Connected to Spotify API');
          $form['connection_success'] = [
              '#type' => 'markup',
              '#markup' => 'Connected to Spotify API',
              '#weight' => '0',
          ];
      }

      $form['client_id'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Client ID'),
          '#description' => $this->t('Spotify Client ID'),
          '#maxlength' => 64,
          '#size' => 64,
          '#default_value' => $config->get('client_id'),
      ];
      $form['client_secret'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Client Secret'),
          '#description' => $this->t('Spotify Client Secret'),
          '#maxlength' => 64,
          '#size' => 64,
          '#default_value' => $config->get('client_secret'),
      ];


    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('cyberduck_spotify.spotifyconfig')
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->save();
  }



}
