<?php

namespace Drupal\cyberduck_spotify\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'ArtistSelectionBlock' block.
 *
 * @Block(
 *  id = "artist_selection_block",
 *  admin_label = @Translation("Artist selection block"),
 * )
 */
class ArtistSelectionBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\cyberduck_spotify\Service\SpotifyFetchService definition.
   *
   * @var \Drupal\cyberduck_spotify\Service\SpotifyFetchService
   */
  protected $cyberduckSpotifySpotifyApi;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->cyberduckSpotifySpotifyApi = $container->get('cyberduck_spotify.spotify_api');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'amount_of_artists_to_display' => 1,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

      if ($this->cyberduckSpotifySpotifyApi->checkConnection()) {
          $form['amount_of_artists_to_display'] = [
              '#type' => 'number',
              '#title' => $this->t('Amount of artists to display'),
              '#description' => $this->t('Show how many artists the user can select from (Max 20)'),
              '#min' => 1,
              '#max' => 20,
              '#default_value' => $this->configuration['amount_of_artists_to_display'],
              '#weight' => '0',
          ];
      } else {
          $form['connection_failed'] = [
              '#type' => 'markup',
              '#markup' => 'Connection to Spotify API failed, <a href="/admin/config/cyberduck-spotify-config"> please check your credentials</a> are correct',
              '#weight' => '0',
          ];
      }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['amount_of_artists_to_display'] = $form_state->getValue('amount_of_artists_to_display');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build = [];
    $build['#theme'] = 'artist_selection_block';
    $build['#cache']['max-age'] = 0;

    if ($this->cyberduckSpotifySpotifyApi->checkConnection()) {

        $limit = $this->configuration['amount_of_artists_to_display'];
        $artists = $this->cyberduckSpotifySpotifyApi->fetchArtists($limit);
        $build['#artists'] = $artists->artists->items;

    }

    return $build;

  }

}
