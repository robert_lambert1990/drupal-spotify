<?php

namespace Drupal\cyberduck_spotify\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
      if ($route = $collection->get('cyberduck_spotify.spotify_artist_controller_start')) {
          $route->setRequirement('_custom_access', 'cyberduck_spotify.access_checker::access');
      }
  }
}
