<?php

namespace Drupal\cyberduck_spotify\Service;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Config\ConfigManagerInterface;

/**
 * Class SpotifyFetchService.
 */
class SpotifyFetchService {

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */

  /**
   * Drupal\Core\Config\ConfigManagerInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;
  protected $httpClient;
  protected $spotify_token;

  /**
   * Constructs a new SpotifyFetchService object.
   */
  public function __construct(ClientInterface $http_client, ConfigManagerInterface $config_manager) {
    $this->httpClient = $http_client;
    $this->configManager = $config_manager;
    $this->connectToSpotifyAPI();
  }

  private function connectToSpotifyAPI() {

     $credentials =  $this->getCredentials();

      try {
          $request = $this->httpClient->request('POST', 'https://accounts.spotify.com/api/token', [
              'form_params' => [
                  "grant_type" => "client_credentials",
              ],
              'headers' => [
                  "Authorization" => "Basic " . $credentials
              ]
          ]);
      } catch (\Exception $e) {
          $request = NULL;
      }

      if ($request) {

          $response = $request->getBody()->getContents();
          $token = json_decode($response);
          $this->spotify_token = $token->access_token;

      }

  }

  private function getCredentials() {

      $config = $this->configManager->getConfigFactory()->get('cyberduck_spotify.spotifyconfig');
      $client_id = $config->get('client_id');
      $client_secret = $config->get('client_secret');

      $credentials = base64_encode($client_id . ':' . $client_secret);
      return $credentials;

  }
  public function checkConnection() {

      if ($this->spotify_token) {
          return TRUE;
      }
  }

  public function fetchArtists($limit = 1) {

      try {

          $request = $this->httpClient->request('GET', 'https://api.spotify.com/v1/search', [
              'headers' => [
                  'Authorization' => 'Bearer ' . $this->spotify_token
              ],
              'query' => [
                  'q' => 'duck',
                  'type' => 'artist',
                  'limit' => $limit
              ]
          ]);

      } catch (\Exception $e) {
          \Drupal::logger('cyberduck_spotify')->error('Failed to fetch Artists from Spotify API');
          $request = NULL;
      }

      if ($request) {

          $response = $request->getBody()->getContents();
          $artists = json_decode($response);
          return $artists;

      }

  }

  public function fetchArtistInformation($id) {

      try {

          $request = $this->httpClient->request('GET', 'https://api.spotify.com/v1/artists/' . $id, [
              'headers' => [
                  'Authorization' => 'Bearer ' . $this->spotify_token
              ],
          ]);

      } catch (\Exception $e) {

          \Drupal::logger('cyberduck_spotify')->error('Failed to fetch Artist Information @id',
              [
                  '@id' => $id,
              ]);
          $request = NULL;
      }

      if ($request) {

          $response = $request->getBody()->getContents();
          $artist = json_decode($response);
          return $artist;

      }
      
    }

}
